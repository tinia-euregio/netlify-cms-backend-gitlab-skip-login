# GitLab backend with login skip

An abstraction layer between the CMS and [GitLab](https://docs.gitlab.com/ee/api/README.html), but w/o authentication.

## Code structure

`Implementation` for [File Management System API](https://github.com/netlify/netlify-cms/tree/master/packages/netlify-cms-lib-util/README.md) based on `Api`. With [Editorial Workflow](https://www.netlifycms.org/docs/beta-features/#gitlab-and-bitbucket-editorial-workflow-support) uses merge requests labels to track unpublished entries statuses.

`Api` - A wrapper for GitLab REST API.

`AuthenticationPage` - A component borrowed from https://github.com/netlify/netlify-cms/blob/master/packages/netlify-cms-backend-test/src/AuthenticationPage.js which allow skip login screen.

Look at tests or types for more info.

## How you should use this

This backend has no authentication layer, so you should serve Netlify CMS behind your own authentication page, and proxy the GitLab API with a software layer that adds the required client secret (authorization token).

Add this to your `<head>`:

```html
<!-- Include the script that enables GitLab backend with login skip -->
<script src="js/netlify-cms-backend-gitlab-skip-login.js"></script>
```

and this to your body after you have loaded `js/netlify-cms.js`:

```html
<script>
  CMS.registerBackend('gitlab-skip-login', NetlifyCmsBackendGitlabSkipLogin.GitLabBackend);
</script>
```

Example Netlify CMS configuration snippet:

```yaml
backend:
  name: gitlab-skip-login
  repo: username/reponame
  branch: main
  api_root: https://my.proxy.example.com/gitlab.com/api/v4
```

## Relation with upstream

This repo is a partial fork from https://github.com/netlify/netlify-cms, obtained with this procedure:

```sh
git clone git@gitlab.com:tinia-euregio/netlify-cms-backend-gitlab-skip-login.git
cd netlify-cms-backend-gitlab-skip-login/
git remote add netlify-cms https://github.com/netlify/netlify-cms
git fetch netlify-cms 
git checkout -b source-master netlify-cms/master
git filter-branch --subdirectory-filter packages -- --all 
CMD="rm -rf netlify-cms netlify-cms-lib-auth netlify-cms-widget-datetime netlify-cms-app netlify-cms-lib-util netlify-cms-widget-file netlify-cms-backend-azure netlify-cms-lib-widgets netlify-cms-widget-image netlify-cms-backend-bitbucket netlify-cms-locales netlify-cms-widget-list netlify-cms-backend-git-gateway netlify-cms-media-library-cloudinary netlify-cms-widget-map netlify-cms-backend-github netlify-cms-media-library-uploadcare netlify-cms-widget-markdown netlify-cms-proxy-server netlify-cms-widget-number netlify-cms-backend-proxy netlify-cms-ui-default netlify-cms-widget-object netlify-cms-backend-test netlify-cms-widget-boolean netlify-cms-widget-relation netlify-cms-core netlify-cms-widget-code netlify-cms-widget-select netlify-cms-default-exports netlify-cms-widget-colorstring netlify-cms-widget-string netlify-cms-editor-component-image netlify-cms-widget-date netlify-cms-widget-text"
git filter-branch -f --tree-filter "$CMD"
git filter-branch -f --subdirectory-filter netlify-cms-backend-gitlab -- --all 
git remote delete netlify-cms
```

# License

Netlify CMS is released under the [MIT License](LICENSE).
Please make sure you understand its [implications and guarantees](https://writing.kemitchell.com/2016/09/21/MIT-License-Line-by-Line.html).
