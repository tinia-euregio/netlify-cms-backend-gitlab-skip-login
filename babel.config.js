const path = require('path');

const isProduction = process.env.NODE_ENV === 'production';
const isTest = process.env.NODE_ENV === 'test';
const isESM = process.env.NODE_ENV === 'esm';

console.log('Build Package:', path.basename(process.cwd()));

const defaultPlugins = [
  'lodash',
  [
    'babel-plugin-transform-builtin-extend',
    {
      globals: ['Error'],
    },
  ],
  'transform-export-extensions',
  '@babel/plugin-proposal-class-properties',
  '@babel/plugin-proposal-object-rest-spread',
  '@babel/plugin-proposal-export-default-from',
  '@babel/plugin-proposal-nullish-coalescing-operator',
  '@babel/plugin-proposal-optional-chaining',
  '@babel/plugin-syntax-dynamic-import',
  'babel-plugin-inline-json-import',
  [
    "@babel/plugin-transform-runtime",
    {
      "regenerator": true,
      "corejs": 3
    }
  ]
];

function presets() {
  return [
    '@babel/preset-react',
    '@babel/preset-env',
    [
      '@emotion/babel-preset-css-prop',
      {
        autoLabel: true,
      },
    ],
    '@babel/typescript',
  ];
}

function plugins() {
  if (isESM) {
    return [
      ...defaultPlugins,
    ];
  }

  if (isTest) {
    return [
      ...defaultPlugins,
    ];
  }

  if (!isProduction) {
    return [...defaultPlugins, 'react-hot-loader/babel'];
  }

  return defaultPlugins;
}

module.exports = {
  presets: presets(),
  plugins: plugins(),
};
